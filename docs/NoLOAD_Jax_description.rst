NoLOAD_Jax structure
====================

The structure of the library is described on the following class diagram:

.. figure::  images/class_diagram.png
   :align:   center
   :scale:   40%

The description for each module of NoLOAD_Jax is below.

analyse
-------
.. toctree::
   :maxdepth: 2

   api/simulation

gui
---
.. toctree::
   :maxdepth: 2

   api/gui1
   api/OpenGUI
   api/plotIterations
   api/plotPareto

numerical_methods
-----------------
interpolation
~~~~~~~~~~~~~
.. toctree::
   :maxdepth: 2

   api/interpolate1D
   api/interpolate2D

ODE
~~~
.. toctree::
   :maxdepth: 2

   api/ode44
   api/ode45
   api/ode45_extract
   api/ode_extracted_features
   api/ode_tools

optimization
------------
.. toctree::
   :maxdepth: 2

   api/ExportToXML
   api/iterationHandler
   api/multiobjective
   api/optimProblem
   api/paretoTools
   api/specifications
   api/Tools
   api/wrapper
