interpolate1D
=============

.. automodule:: noloadj.numerical_methods.interpolation.interpolate1D
    :members:
    :show-inheritance:
