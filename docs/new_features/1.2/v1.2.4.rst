What’s new in version 1.2.4
===========================
Version available since March 3rd 2023


New Features
------------

- For periodic systems, derivatives of average and RMS values are computed according to period.

Bug fixed
---------

- "variable" parameter in OpitmizeParam function lead to failing execution.

Contributors
------------
Lucas Agobert
