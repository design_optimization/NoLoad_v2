.. noload documentation master file, created by
   sphinx-quickstart on Fri May 28 17:14:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NoLOAD_Jax's documentation!
======================================

Introduction to NoLOAD_Jax
--------------------------
NoLOAD_Jax stands for Non-Linear Optimization using Automatic Differentiation
with Jax package.
It aims to be a light non-linear optimization tool for energy components and
systems.
It is an Open Source project located on GitLab at `NoLOAD Gitlab`_, with
available open benchmark examples at `NoLOAD benchmark Gitlab`_

Contents
--------
.. toctree::
   :maxdepth: 2

   about_NoLOAD_Jax

.. toctree::
   :maxdepth: 1

   installation_requirements

.. toctree::
   :maxdepth: 2

   how_to_NoLOAD_Jax

.. toctree::
   :maxdepth: 2

   dynamic_systems_optim

.. toctree::
   :maxdepth: 2

   NoLOAD_Jax_description

.. toctree::
   :maxdepth: 2

   new_functionnalities

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Acknowledgments
---------------

A special thank is adressed to the contributors that are not main authors, such as Manh Tung Phan for the IHM (Human-Machine Interaction.)


.. _NoLOAD Gitlab: https://gricad-gitlab.univ-grenoble-alpes.fr/design_optimization/NoLoad_v2
.. _NoLOAD benchmark Gitlab: https://gricad-gitlab.univ-grenoble-alpes.fr/design_optimization/noload_benchmarks_open/-/tree/noload_version2
