What's new in the latest versions
=================================

The new version of NoLOAD_Jax v1.3.9 is available!
The release is from 5th of July 2024.

.. toctree::
   :maxdepth: 2

   new_features/v1.3.9


Information about the latest versions
-------------------------------------

.. toctree::
   :maxdepth: 1

   new_features/v1.3.8
   new_features/v1.3.7
   new_features/v1.3.6
   new_features/v1.3.5
   new_features/v1.3.4
   new_features/v1.3.3
   new_features/v1.3.2
   new_features/v1.3.1
   new_features/v1.3.0
   new_features/1.2/v1.2.9
   new_features/1.2/v1.2.8
   new_features/1.2/v1.2.7
   new_features/1.2/v1.2.6
   new_features/1.2/v1.2.5
   new_features/1.2/v1.2.4
   new_features/1.2/v1.2.3
   new_features/1.2/v1.2.2
   new_features/1.2/v1.2.15
   new_features/1.2/v1.2.1
   new_features/1.2/v1.2.0
   new_features/1.1/v1.1.9
   new_features/1.1/v1.1.8
   new_features/1.1/v1.1.7
   new_features/1.1/v1.1.6
   new_features/1.1/v1.1.5
   new_features/1.1/v1.1.41
   new_features/1.1/v1.1.3
   new_features/1.1/v1.1.2
   new_features/1.1/v1.1.1
   new_features/1.1/v1.1.0
   new_features/1.0/v1.0.2
   new_features/1.0/v1.0.1



