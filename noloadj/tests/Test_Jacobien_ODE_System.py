import jax.numpy as np
from jax import jacfwd
from noloadj.numerical_methods.ODE.ode45 import odeint45
from noloadj.numerical_methods.ODE.ode45_extract import odeint45_extract,\
    final_time,threshold,steady_state
from datetime import datetime
from numpy.testing import *
from noloadj.numerical_methods.ODE.ode_tools import ODESystem
from noloadj.numerical_methods.ODE.ode_extracted_features import moy

# CALCUL + JACOBIEN

#calcul
k=np.array([1.])

class ODE(ODESystem):

    def __init__(self,stopping_criteria=1.):
        ODESystem.__init__(self)
        self.xnames=['z']
        self.ynames = ['z']
        if stopping_criteria==1:
            self.stop=final_time(10.)
        elif stopping_criteria==2:
            self.stop=threshold('z',1e-5)
        else:
            self.stop=steady_state(a=1e-5)
        self.constraints={'moy_z':moy('z')}

    def timederivatives(self,x,t,*P):
        k,=P
        return -k*x

    def output(self,x,t,*P):
        k,=P
        return x

def f(k):
    x,y=odeint45(ODE(),np.array([1.]),np.linspace(0.,10.,100000),k,h0=1e-4)
    return np.array([x[0][-1]]),np.mean(x[0])

def f2(k):
    tf,Xf,Yf,cstr=odeint45_extract(ODE(),np.array([1.]),k,h0=1e-4)
    return Xf[0],cstr['moy_z']

def f3(k):
    tf,Xf,Yf,cstr=odeint45_extract(ODE(2.),np.array([1.]),k,h0=1e-4)
    return Xf[0],cstr['moy_z']

def f4(k):
    tf,Xf,Yf,cstr=odeint45_extract(ODE(3.),np.array([1.]),k,h0=1e-4)
    return Xf[0],cstr['moy_z']

class Test_Jacobien_ODE_System(TestCase):

    def test_odeint45(self):
        start_time_execution = datetime.today()
        j,moy_z=jacfwd(f)(k)
        analytical_jac=-10.*np.exp(-10.)
        analytical_moy_z=(np.exp(-k*10.)-1.)/10.
        print(j)
        print('temps de calcul : ' + str(datetime.today()-start_time_execution))

        # confirm jacobian
        assert np.all(np.isclose(j, analytical_jac,atol=1e-5))
        assert np.all(np.isclose(moy_z, analytical_moy_z, atol=1e-4))

    def test_odeint45_extract_final_time(self):
        start_time_execution = datetime.today()
        j,moy_z=jacfwd(f2)(k)
        tf=10.
        analytical_jac=-tf*np.exp(-tf)
        analytical_moy_z=(np.exp(-k*tf)-1.)/tf
        print(j)
        print('temps de calcul : ' + str(datetime.today()-start_time_execution))

        # confirm jacobian
        assert np.all(np.isclose(j, analytical_jac,atol=1e-5))
        assert np.all(np.isclose(moy_z, analytical_moy_z, atol=1e-4))

    def test_odeint45_extract_threshold(self):
        start_time_execution = datetime.today()
        j,moy_z=jacfwd(f3)(k)
        tf=11.566# final time with threshold value stopping criteria
        analytical_jac=-tf*np.exp(-tf)
        analytical_moy_z=(np.exp(-k*tf)-1.)/tf
        print(j)
        print('temps de calcul : ' + str(datetime.today()-start_time_execution))

        # confirm jacobian
        assert np.all(np.isclose(j, analytical_jac,atol=1e-5))
        assert np.all(np.isclose(moy_z, analytical_moy_z, atol=1e-4))

    def test_odeint45_extract_steady_state(self):
        start_time_execution = datetime.today()
        j,moy_z=jacfwd(f4)(k)
        tf=12.172# final time with steady_state stopping criteria
        analytical_jac=-tf*np.exp(-tf)
        analytical_moy_z=(np.exp(-k*tf)-1.)/tf
        print(j)
        print('temps de calcul : ' + str(datetime.today()-start_time_execution))

        # confirm jacobian
        assert np.all(np.isclose(j, analytical_jac,atol=1e-5))
        assert np.all(np.isclose(moy_z, analytical_moy_z, atol=1e-4))
