from noloadj.analyse.simulation import computeOnce,computeJacobian
from noloadj.numerical_methods.ODE.ode45_extract import odeint45_extract,\
    steady_state
from datetime import datetime
from numpy.testing import *
from noloadj.numerical_methods.ODE.ode_tools import *
from noloadj.numerical_methods.ODE.ode_extracted_features import *

# CALCUL + JACOBIEN

R=15
L=0.002
C=1e-4
Ve=12
a=0.2
T=1/5000
tf=20*T
pas=1e-7
nb=int(tf/pas)
time=np.linspace(0,tf,nb)
inputs={'L':L,'C':C}
outputs=['vc_min','vc_max','vc_moy','vc_eff','vc_fond','vc_thd']
M=int(T/pas)

class boost(ODESystem):

    def __init__(self,Ve,R,alpha,T):
        ODESystem.__init__(self)
        self.Ve=Ve
        self.R=R
        self.aT=alpha*T

        self.state=1
        self.xnames=['vc','il']
        self.ynames=['id']

        self.stop=steady_state(5,self.xnames,1e-5)
        self.constraints={'vc_min':Min('vc'),'vc_max':Max('vc'),
            'vc_moy':moy('vc'),'vc_eff':eff('vc'),'vc_fond':Module_FFT('vc',1),
            'vc_thd':THD('vc')}

        self.commande=Creneau(alpha,T)

    def timederivatives(self,x,t,*P):
        C,L=P
        def state0():
            vc=x[0]
            vc_dot=-vc/(self.R*C)
            return np.array([vc_dot,0.])
        def state1():
            vc,il=x[0],x[1]
            vc_dot=-vc/(self.R*C)
            il_dot=self.Ve/L
            return np.array([vc_dot,il_dot])
        def state2():
            vc,il=x[0],x[1]
            vc_dot=(il-vc/self.R)/C
            il_dot=(self.Ve-vc)/L
            return np.array([vc_dot,il_dot])
        return Switch(self.state,[state0,state1,state2])

    def output(self,x,t,*P):
        C,L=P
        il=x[1]
        def state0():
            vc=x[0]
            il=0.
            id=0.
            return np.array([vc,il]),np.array([id])
        def state1():
            id=0.
            return x,np.array([id])
        def state2():
            id=il
            return x,np.array([id])
        return Switch(self.state,[state0,state1,state2])


    def update(self,x,y,t,c):
        eps,nstate,nx,ny=1e-10,self.state,x,y
        id=ny[0]
        def state0():
            def to_state_1(state):
                nstate,nx,ny=state
                return 1,nx,ny
            return Condition([c==1],[to_state_1],(nstate,nx,ny))
        def state1():
            def to_state_2(state):
                nstate,nx,ny=state
                return 2,nx,ny
            return Condition([c==0],[to_state_2],(nstate,nx,ny))
        def state2():
            def to_state_0(state):
                nstate,nx,ny=state
                vc=nx[0]
                il=0.
                id=0.
                return 0,np.array([vc,il]),np.array([id])
            def to_state_1(state):
                nstate,nx,ny=state
                return 1,nx,ny
            return Condition([id<eps,c==1],[to_state_0,to_state_1],(nstate,nx,
                                                                    ny))
        return Switch(self.state,[state0,state1,state2])


def model(L,C):
    Boost=boost(Ve,R,a,T)
    tchoc,_,_,cstr,_=odeint45_extract(Boost,np.array([0.,0.]),C,L,T=T,M=M,
                                      h0=1e-7)
    vc_min=cstr['vc_min']
    vc_max=cstr['vc_max']
    vc_moy=cstr['vc_moy']
    vc_eff=cstr['vc_eff']
    vc_fond=cstr['vc_fond']
    vc_thd=cstr['vc_thd']
    return locals().items()

class Test_Jacobien_ODE_criteria(TestCase):

    def test_jacobian_ODE_criteria(self):
        eps=1e-6
        start_time_execution = datetime.today()
        dvc_min,dvc_max,dvc_moy,dvc_eff,dvc_fond,dvc_thd= computeJacobian\
            (model,inputs,outputs)
        print('temps de calcul : ' + str(datetime.today()-start_time_execution))
        vc_min1,vc_max1,vc_moy1,vc_eff1,vc_fond1,vc_thd1= computeOnce(model,
                                                                inputs,outputs)
        inputs['L']*=(1.+eps)
        vc_min2,vc_max2,vc_moy2,vc_eff2,vc_fond2,vc_thd2=computeOnce(model,
                                                            inputs,outputs)
        analytical_dvc_min=(vc_min2-vc_min1)/(eps*L)
        analytical_dvc_max=(vc_max2-vc_max1)/(eps*L)
        analytical_dvc_moy=(vc_moy2-vc_moy1)/(eps*L)
        analytical_dvc_eff=(vc_eff2-vc_eff1)/(eps*L)
        analytical_dvc_fond=(vc_fond2-vc_fond1)/(eps*L)
        analytical_dvc_thd=(vc_thd2-vc_thd1)/(eps*L)

        # confirm jacobian
        print(dvc_min[0], analytical_dvc_min)
        print(dvc_max[0], analytical_dvc_max)
        print(dvc_moy[0], analytical_dvc_moy)
        print(dvc_eff[0], analytical_dvc_eff)
        print(dvc_fond[0], analytical_dvc_fond)
        print(dvc_thd[0], analytical_dvc_thd)

        assert np.all(np.isclose(dvc_min[0], analytical_dvc_min, rtol=1e-1))
        assert np.all(np.isclose(dvc_max[0], analytical_dvc_max, atol=1e-2))
        assert np.all(np.isclose(dvc_moy[0], analytical_dvc_moy, atol=1e-2))
        assert np.all(np.isclose(dvc_eff[0], analytical_dvc_eff, atol=1e-2))
        assert np.all(np.isclose(dvc_fond[0], analytical_dvc_fond, atol=1e-2))
        assert np.all(np.isclose(dvc_thd[0], analytical_dvc_thd, atol=1e-2))