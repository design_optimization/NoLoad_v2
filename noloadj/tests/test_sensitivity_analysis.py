# SPDX-FileCopyrightText: 2021 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

from numpy.testing import *
from noloadj.analyse.simulation import computeOnce,computeJacobian,\
    computeParametric
from noloadj.analyse.DGSM import computeDGSM
from noloadj.analyse.Sobol import computeSobol
import numpy as np
import jax.numpy as jnp

def rosenbrock(x,y):
    fobj=(1-x)*(1-x)+100*(y-x*x)**2
    ctr1=(x-1)**3-y+1
    ctr2=x+y-2
    return locals().items()
    
def Ishigami(x1,x2,x3,A,B):
    Y=jnp.sin(x1)+A*jnp.sin(x2)*jnp.sin(x2)+B*jnp.sin(x1)*(x3**4)
    return locals().items()

class test_sensitivity_analysis(TestCase):
    def test_sensitivity(self):
        decimal=4

        # Compute outputs
        inputs = {'x': 1.0, 'y': 2.0}
        outputs = ['fobj', 'ctr1', 'ctr2']
        results = computeOnce(model=rosenbrock, inputs=inputs, outputs=outputs)
        print(outputs, '=', results)

        assert_almost_equal(results, [100.0, -1.0, 1.0], decimal)
        print('\n')

        # Compute Parametric
        inputs = {'y': 2.0}
        outputs = ['fobj', 'ctr1', 'ctr2']

        variable = 'x'
        values = np.arange(-1.5, 1.5, 0.1)  # [-1.5, -1.4, ..., 1.5]
        iter = computeParametric(rosenbrock, variable, values, inputs, outputs)

        df = iter.print()
        print(df)

        fobj_ref=[12.5,5.92,14.9,36.2,66.82,104,145.22,188.2,230.9,271.52,308.5,
                  340.52,366.5,385.6,397.22,401,396.82,384.8,365.3,338.92,306.5,
                  269.12,228.1,185,141.62,100,62.42,31.4,9.7,0.32]
        assert_almost_equal(df['fobj'], fobj_ref, decimal)
        print('\n')

        # Compute Gradients
        inputs = {'x': 1.0, 'y': 2.0}
        outputs = ['fobj', 'ctr1', 'ctr2']
        dfobj,dctr1,dctr2 = computeJacobian(model=rosenbrock, inputs=inputs,
                                        outputs=outputs)
        print('dfobj =', dfobj)
        print('dctr1 =', dctr1)
        print('dctr2 =', dctr2)

        assert_almost_equal(dfobj, [-400.,200.], decimal)
        assert_almost_equal(dctr1, [0.,-1.], decimal)
        assert_almost_equal(dctr2, [1.,1.], decimal)
    def test_Sobol(self): # approximatif : a revoir
        decimal = 2

        inputs = {'x1': 0., 'x2': 0., 'x3': 0.}
        deltas = {'x1': jnp.pi, 'x2': jnp.pi, 'x3': jnp.pi}
        deltas_type = {'x1': 'abs', 'x2': 'abs', 'x3': 'abs'}
        outputs = ['Y']
        Param = {'A': 7., 'B': 0.1}
        DGSM = computeDGSM(model=Ishigami, inputs=inputs, outputs=outputs,
                     deltas=deltas, deltas_type=deltas_type,N=1024,Param=Param)
        print('DGSM=', DGSM)

        assert_almost_equal(DGSM['Y']['x1'], 2.23, decimal)
        assert_almost_equal(DGSM['Y']['x2'], 7.06, decimal)
        assert_almost_equal(DGSM['Y']['x3'], 3.17, decimal)

        inputs = {'x1': 0., 'x2': 0., 'x3': 0.}
        deltas = {'x1': jnp.pi, 'x2': jnp.pi, 'x3': jnp.pi}
        deltas_type = {'x1': 'abs', 'x2': 'abs', 'x3': 'abs'}
        outputs = ['Y']
        Param = {'A': 7., 'B': 0.1}
        Sobol = computeSobol(model=Ishigami, inputs=inputs, outputs=outputs,
                    deltas=deltas, deltas_type=deltas_type,N=1024,Param=Param)
        print('Sobol=', Sobol)

        assert_almost_equal(Sobol['Y']['x1'], 0.31, decimal)
        assert_almost_equal(Sobol['Y']['x2'], 0.44, decimal)
        assert_almost_equal(Sobol['Y']['x3'], 0.01, decimal)

