<!--
SPDX-FileCopyrightText: 2021 G2Elab / MAGE

SPDX-License-Identifier: Apache-2.0
-->

Authors: 
=======
 B. DELINCHANT, L. GERBAUD, F. WURTZ

Main Contributors: 
=================
Lucas Agobert

Contributors: 
=============
Sacha Hodencq